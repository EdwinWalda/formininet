from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.util import irange,dumpNodeConnections
from mininet.log import setLogLevel

class CustomTopo(Topo):
	"Simple Data Center Topology"

	"linkopts - (1:core, 2:aggregation, 3: edge) parameters"
	"fanout - number of child switch per parent switch"
	def __init__(self, linkopts1, linkopts2, linkopts3, fanout=2, **opts):
		# Initialize topology and default options
		Topo.__init__(self, **opts)
		
		# Add your logic here ...
		self.fanout = fanout

		counter = 0
		hostCounter = 0

		core = self.addSwitch('c0')
		for i in xrange(0, fanout):
			switch = self.addSwitch('s%s' % counter)
			self.addLink(switch, core, bw=10)
			counter += 1
			for i in xrange(0, fanout):
				edge = self.addSwitch('s%s' % counter)
				counter += 1
				self.addLink(switch, edge)
				for i in xrange(0, fanout):
					host = self.addHost('h%s' % hostCounter)
					hostCounter += 1
					self.addLink(host,edge)
			

linkopts1 = dict(bw =10, delay='5ms', loss=10, max_queue_size=1000, use_htb=True)
linkopts2 = dict(bw =10, delay='5ms', loss=10, max_queue_size=1000, use_htb=True)
linkopts3 = dict(bw =10, delay='5ms', loss=10, max_queue_size=1000, use_htb=True)
					
topos = { 'custom1': ( lambda: CustomTopo(linkopts1,linkopts2,linkopts3) )}
