from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpidToStr
from pox.lib.addresses import EthAddr
from collections import namedtuple
import os
import csv
''' Add your imports here ... '''



log = core.getLogger()
policyFile = "%s/pox/pox/misc/firewall-policies.csv" % os.environ[ 'HOME' ]  

''' Add your global variables here ... '''



class Firewall (EventMixin):

    def __init__ (self):
        self.listenTo(core.openflow)
        log.debug("Enabling Firewall Module")

    def _handle_ConnectionUp (self, event):    
        ''' Add your logic here ... '''

        with open(policyFile,'r') as csvfile:
            ourReader = csv.DictReader(csvfile)
            for row in ourReader:
                
                print row['mac_0'], row['mac_1']
                msg = of.ofp_flow_mod()
                msg.priority = int(row['id'])
                my_match = of.ofp_match(dl_src = EthAddr(row['mac_0']), dl_dst = EthAddr(row['mac_1']))
                msg.match = my_match
                core.openflow.sendToDPID(event.dpid, msg)
                msg2 = of.ofp_flow_mod()
                msg2.priority = int(row['id'])
                my_match2 = of.ofp_match(dl_src = EthAddr(row['mac_1']), dl_dst = EthAddr(row['mac_0']))
                msg2.match = my_match2
                core.openflow.sendToDPID(event.dpid, msg2)
        

    
        log.debug("Firewall rules installed on %s", dpidToStr(event.dpid))

def launch ():
    '''
    Starting the Firewall module
    '''
    core.registerNew(Firewall)
