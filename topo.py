from mininet.topo import Topo
from mininet.link import TCLink
from mininet.net import Mininet
import csv
import os


delayFile = "%s/pox/pox/misc/delay.csv" % os.environ[ 'HOME' ]  

class Q9Topo(Topo):
    def __init__(self, **opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)
        
        # Add your logic here ...
        
        s11 = self.addSwitch('s11')
        s12 = self.addSwitch('s12')
        s14 = self.addSwitch('s14')
        s16 = self.addSwitch('s16')
        s18 = self.addSwitch('s18')
        h13 = self.addHost('h13')
        h15 = self.addHost('h15')
        h17 = self.addHost('h17')  
        h19 = self.addHost('h19')

        linkDict = {
        	'g':(s11,s12),
        	'h':(s12,s14),
        	'i':(s14,s16),
        	'j':(s18,s16),
        	'k':(s11,s18),
        	'l':(s18,s12),
        	'm':(s12,s16),
        	'n':(s14,s18),
        }

        self.addLink(h13,s12)
        self.addLink(h15,s14)
        self.addLink(h19,s18)
        self.addLink(h17,s16)

        with open(delayFile, 'r') as csvfile:
        	ourReader = csv.DictReader(csvfile)
        	for row in ourReader:
        		linkName = row['link']
        		self.addLink(linkDict[linkName][0],linkDict[linkName][1],delay=row['delay'])
                   
topos = { 'custom': ( lambda: Q9Topo() ) }