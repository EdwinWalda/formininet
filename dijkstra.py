from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
from pox.lib.util import dpidToStr
from pox.lib.addresses import EthAddr
from collections import namedtuple
import os
import csv
import math
''' Add your imports here ... '''



log = core.getLogger()
delayFile = "delay.csv"

''' Add your global variables here ... '''

class Dijkstra (EventMixin):

    def __init__ (self):
        self.listenTo(core.openflow)
        log.debug("Enabling Dijkstra Module")

        with open(delayFile,'r') as csvfile:
            ourReader = csv.DictReader(csvfile)
            for row in ourReader:

                link = row['link']
                delay = row['delay']

                self.delayList[link] = delay

         network =   {'s11': {'s12': self.delayList["g"], 
                              's18': self.delayList["k"]},

            's12': {'h13': 1, 's14': self.delayList["h"], 
                              's16': self.delayList["m"], 
                              's18': self.delayList["l"]},

            's14': {'h15': 1, 's12': self.delayList["h"], 
                              's18': self.delayList["n"], 
                              's16': self.delayList["i"]},

            's16': {'h17': 1, 's12': self.delayList["m"], 
                              's14': self.delayList["i"], 
                              's18': self.delayList["j"]},

            's18': {'h19': 1, 's12': self.delayList["l"], 
                              's16': self.delayList["j"], 
                              's14': self.delayList["n"]},

            'h13': {'s12': 0},

            'h15': {'s14': 0},

            'h17': {'s16': 0},

            'h19': {'s18': 0},
            }

    def calculate(self, src, dest):
        distSwitch = {
            's12': math.inf
            's14': math.inf
            's11': math.inf
            's18': math.inf
            's16': math.inf
            }

        prevSwitch = {
            's12': None
            's14': None
            's11': None
            's18': None
            's16': None
        }

        touched = {
            's12': False
            's14': False
            's11': False
            's18': False
            's16': False
        }

        check = True
        currentNode = src
        touched[src] = True
        for key, value in network[src]:
            distSwitch[key] = value
            prevSwitch[key] = src

        while check:
            length = math.inf
            for key, value in network[currentNode]
                if distSwitch[key] < length and touched[key] == False:
                    currentNode = key

            touched[currentNode] = True

            for key, value in network[currentNode]
                newLength = distSwitch[currentNode] + value
                if newLength < distSwitch[key]:
                    distSwitch[key] = newLength
                    prevSwitch[key] = currentNode

            check = False
            for key, value in touched:
                if value == False:
                    check = True

            


    def _handle_ConnectionUp (self, event):    
        for node in network.keys():
            for node2 in network.keys():
                calculate(node, node2)
                ## set rules for calculated path ##
        
        

        log.debug("Dijkstra installed on %s", dpidToStr(event.dpid))        

def launch ():
    '''
    Starting the Dijkstra module
    '''
    core.registerNew(Dijkstra)
